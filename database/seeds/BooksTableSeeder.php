<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([[
            'id'=>1,
            'title' => 'Gone for good',
            'user_id' => '1',
            'author' => 'Harlen Coben',
            'created_at'=>date('Y-m-d G:i:s'),
        ],
       [
            'id'=>2,
            'title' => 'My sisters keeper',
            'user_id' => '1',
            'author' => 'Jodi picoult',
            'created_at'=>date('Y-m-d G:i:s'),  
       ],
       [
            'id'=>3,
            'title' => 'Six years',
            'user_id' => '1',
            'author' => 'Harlen Coben',
            'created_at'=>date('Y-m-d G:i:s'),
       ],
        [
            'id'=>4,
            'title' => 'Long lost',
            'user_id' => '2',
            'author' => 'Harlen Coben',
            'created_at'=>date('Y-m-d G:i:s'),
        ],
        [
            'id'=>5,
            'title' => 'Home',
            'user_id' => '2',
            'author' =>'Harlen Coben',
            'created_at'=>date('Y-m-d G:i:s'),
        ],
        ]); 
    }
}